var getDetail = function(charData){
	
	var detailWin = Ti.UI.createWindow({
		title: "Details",
		backgroundColor: "white"
	});
	
	var backButton = Ti.UI.createView({
		backgroundColor: "gray",
		height: 40,
		width: 80,
		top: 10,
		left: 10
	});
	
	var buttonLabel = Ti.UI.createLabel({
		text: " Back ",
		color: "black",
	});
	
	var detailText = Ti.UI.createLabel({
		text: charData.title,
		color: "black",
		font:{
			fontSize: 30
		},
		top: 60,
	});
	
	var detailDesc = Ti.UI.createLabel({
		text: charData.description,
		color: "black",
		font: {
			fontSize: 20
		},
		bottom: 100,
	});
	
	var detailPhoto = Ti.UI.createImageView({
		image: charData.photo,
		height: 300,
		width: 300	
	});
	
	backButton.add(buttonLabel);
	
	backButton.addEventListener("click", function(){
		detailWin.close();
	});
	
	detailWin.add(backButton);
	detailWin.add(detailText);
	detailWin.add(detailDesc);
	detailWin.add(detailPhoto);

	detailWin.open();
};

exports.getDetail = getDetail;