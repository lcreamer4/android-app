exports.data = [{
	title : "Alice",
	description : "Movie: Alice in Wonderland, Date Released: July 26, 1951", 
	leftImage : "DPics/Alice.jpg",
	photo : "DPics/Alice.jpg"

}, {
	title : "Ariel",
	description: "Movie: The Little Mermaid, Date Released: November 17, 1989",
	leftImage : 'DPics/Ariel.jpg',
	photo : 'DPics/Ariel.jpg'
}, {
	title : "Aurora",
	description: "Movie: Sleeping Beauty, Date Released: January 29, 1959",
	leftImage : 'DPics/Aurora.jpg',
	photo: 'DPics/Aurora.jpg'
}, {
	title : "Belle",
	description : "Movie: Beauty and the Beast, Date Released: November 22, 1991",
	leftImage : 'DPics/Belle_2.jpg',
	photo: 'DPics/Belle_2.jpg'
}, {
	title : "Cinderella",
	description: "Movie: Cinderella, Date Released: March 4, 1950",
	leftImage : 'DPics/Cinderella.jpg',
	photo: 'DPics/Cinderella.jpg'
}, {
	title : "Elsa",
	description: "Movie: Frozen, Date Released: November 27, 2013 ",
	leftImage : 'DPics/Elsa.jpg',
	photo:'DPics/Elsa.jpg'
}, {
	title : "Esmeralda",
	description : "Movie: The Hunchback of Notre Dame, Date Released: December 29, 1939",
	leftImage : 'DPics/Esmeralda.jpg',
	photo: 'DPics/Esmeralda.jpg'
}, {
	title : "Jane",
	description : "Movie: Tarzan, Date Released: June 18, 1999 ",
	leftImage : 'DPics/Jane.jpg',
	photo: 'DPics/Jane.jpg'
}, {
	title : "Jasmine",
	description : "Movie: Aladdin, Date Released: November 25, 1992 ",
	leftImage : 'DPics/Jasmine.jpg',
	photo: 'DPics/Jasmine.jpg'
}, {
	title : "Joy",
	description : "Movie: Inside Out, Date Released: June 19 2015 ",
	leftImage : 'DPics/Joy.jpg',
	photo: 'DPics/Joy.jpg'
}, {
	title : "Kida",
	description: "Movie: Atlantis The Lost Empire, Date Released: June 15, 2001 ",
	leftImage : 'DPics/Kida.jpg',
	photo: 'DPics/Kida.jpg'
}, {
	title : "Lilo",
	description: "Movie: Lilo and Stitch, Date Released: June 21, 2002 ",
	leftImage : 'DPics/Lilo.jpg',
	photo: 'DPics/Lilo.jp'
}, {
	title : " Meg",
	description:"Movie: Hercules, Date Released: June 27, 1997 ",
	leftImage : 'DPics/Meg.jpg',
	photo: 'DPics/Meg.jpg'
}, {
	title : "Merida",
	description: "Movie: Brave, Date Released: June 22, 2012 ",
	leftImage : 'DPics/Merida.jpg',
	photo: 'DPics/Merida.jpg'
}, {
	title : "Mulan",
	description: "Movie: Mulan, Date Released: June 19 1998 ",
	leftImage : 'DPics/Mulan.jpg',
	photo: 'DPics/Mulan.jpg'
}, {
	title : "Pocahontas",
	description: "Movie: Pocahontas, Date Released: June 23, 1995 ",
	leftImage : 'DPics/Pocahontas.jpg',
	photo: 'DPics/Pocahontas.jpg'
}, {
	title : "Rapunzel",
	description: "Movie: Tangled, Date Released: November 24, 2010 ",
	leftImage : 'DPics/Rapunzel.jpg',
	photo: 'DPics/Rapunzel.jpg'
}, {
	title : "Snow White",
	description: "Movie: Snow White and the Seven Dwarves, Date Released: December 21, 1937",
	leftImage : 'DPics/SnowWhite.jpg',
	photo: 'DPics/SnowWhite.jpg'
}, {
	title : "Tiana",
	description: "Movie: The Princess and the Frog, Date Released: December 11, 2009 ",
	leftImage : 'DPics/Tiana.jpg',
	photo: 'DPics/Tiana.jpg'
}, {
	title : "Wendy",
	description: "Movie: Peter Pan, Date Released: February 5, 1953 ",
	leftImage : 'DPics/Wendy.jpg',
	photo: 'DPics/Wendy.jpg'

}];
