// Import data
var characterData = require("character_data").data;
var detail = require("detail").getDetail;
var search = require("search").search;
// Create Window

var tableWin = Ti.UI.createWindow({
	 backgroundColor : "white",
	title : "Female Disney Characters",
});

// Create Table Data Empty Array
var tableData = [];

var table = Ti.UI.createTableView({
	top : 50,
	search:search
});

// Loop through data
for (var i = 0, j = characterData.length; i < j; i++) {
	
	var character = characterData[i];

	// For EACH character create a row
	var row = Ti.UI.createTableViewRow({
		testData : character,
		title : character.title,
		color: "black",
		leftImage : character.leftImage,
	});

	// Push the row to the table data
	tableData.push(row);
	
	// Set table data
	table.setData(tableData);
};

// Click Event ON the table
table.addEventListener("click", function(e) {
	
	var charData = e.source.testData;	
	
	var detailsWindow = detail(charData);
});

// Add table to the window
tableWin.add(table);
//Open the table window
tableWin.open();
